FROM --platform=linux/arm64 python:3.10
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY main.py /app/main.py
EXPOSE 9877
CMD /app/main.py

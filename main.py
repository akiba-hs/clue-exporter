#!/usr/bin/env python3

import logging
import os
import time
import serial
from prometheus_client import start_http_server, Gauge

# Logging 
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

SERIAL_PATH = os.environ.get("SERIAL_PATH", "/dev/ttyACM0")

class ClueExporter:
    def __init__(self):
        self.metric_dict = {}

    def get_metrics(self):
        with serial.Serial(SERIAL_PATH, 9600, timeout=1) as s:
            s.reset_input_buffer()
            s.readline()
            d = s.readline()
            print(d)
            data = d.decode().split(" ")
            temp = float(data[0])
            humi = float(data[1])
            sound = float(data[2])
            pressure = float(data[3])
            magnetic_x = float(data[4])
            magnetic_y = float(data[5])
            magnetic_z = float(data[6])
            color_red = int(data[7])
            color_green = int(data[8])
            color_blue = int(data[9])
            color_clear = int(data[10])
            return temp, humi, sound, pressure, magnetic_x, magnetic_y, magnetic_z, color_red, color_green, color_blue, color_clear

    def create_gauge_for_metric(self, metric_name, description):
        if self.metric_dict.get(metric_name) is None:
            self.metric_dict[metric_name] = Gauge(metric_name, description)
    
    def set_value(self, metric_name, value):
        self.metric_dict[metric_name].set(value)

    def main(self):
        exporter_port = int(os.environ.get("EXPORTER_PORT", "9877"))
        start_http_server(exporter_port)
        self.create_gauge_for_metric("clue_temperature", "Adafruit Clue: temperature sensor")
        self.create_gauge_for_metric("clue_humidity", "Adafruit Clue: humidity sensor")
        self.create_gauge_for_metric("clue_sound", "Adafruit Clue: sound level sensor")
        self.create_gauge_for_metric("clue_pressure", "Adafruit Clue: pressure sensor")
        self.create_gauge_for_metric("clue_magnetic_x", "Adafruit Clue: magnetic sensor X")
        self.create_gauge_for_metric("clue_magnetic_y", "Adafruit Clue: magnetic sensor Y")
        self.create_gauge_for_metric("clue_magnetic_z", "Adafruit Clue: magnetic sensor Z")
        self.create_gauge_for_metric("clue_color_red", "Adafruit Clue: light sensor (red)")
        self.create_gauge_for_metric("clue_color_green", "Adafruit Clue: light sensor (green)")
        self.create_gauge_for_metric("clue_color_blue", "Adafruit Clue: light sensor (blue)")
        self.create_gauge_for_metric("clue_color_clear", "Adafruit Clue: light sensor (clear)")
        while True:
            try:
                temp, humi, sound, pressure, magnetic_x, magnetic_y, magnetic_z, color_red, color_green, color_blue, color_clear = self.get_metrics()
                self.set_value("clue_temperature", temp)
                self.set_value("clue_humidity", humi)
                self.set_value("clue_sound", sound)
                self.set_value("clue_pressure", pressure)
                self.set_value("clue_magnetic_x", magnetic_x)
                self.set_value("clue_magnetic_y", magnetic_y)
                self.set_value("clue_magnetic_z", magnetic_z)
                self.set_value("clue_color_red", color_red)
                self.set_value("clue_color_green", color_green)
                self.set_value("clue_color_blue", color_blue)
                self.set_value("clue_color_clear", color_clear)
            except Exception as e:
                logging.error(e)
            time.sleep(10)

if __name__ == "__main__":
    c = ClueExporter()
    c.main()
